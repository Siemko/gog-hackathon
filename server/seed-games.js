const Datastore = require('nedb')
const dbGames = new Datastore({ filename: './game.db', autoload: true })
const _ = require('lodash')
const algoliasearch = require('algoliasearch')
const client = algoliasearch('YN0UGK76HW', '23c007d064009a709392a59037f1d6cf')
const index = client.initIndex('gog_games')
const fetch = require('node-fetch')

const addGamesToDB = async () => {
  let result = []
  let json = await (await fetch('https://api.gog.com/v1/games?locale=pl-PL')).json()
  result.push(json._embedded.items)
  let pages = json.pages
  for (let i = 1; i < pages - 1; i++) {
    json = await (await fetch(json._links.next.href)).json()
    result.push(json._embedded.items)
  }
  const flatten = _.flatten(result)
  const mapped = flatten.filter(item => item._embedded.product.isAvailableInCatalog).map(item => {
    return {
      id: item._embedded.product.id,
      title: item._embedded.product.title,
      image: item._embedded.product._links.image.href.replace('{formatter}', 'product_630'),
      features: item._embedded.features.map(f => f.name),
      background: item._links.backgroundImage.href,
    }
  })
  dbGames.insert(mapped)
  const chunks = _.chunk(mapped, 1000)

  chunks.map(function(batch) {
    return index.addObjects(batch)
  })
}
const once = _.once(addGamesToDB)
once()
