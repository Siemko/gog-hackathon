const fastify = require('fastify')()
const cors = require('cors')
const Datastore = require('nedb')
const dbProblem = new Datastore({ filename: './problem.db', autoload: true })
const dbAnswer = new Datastore({ filename: './answer.db', autoload: true })
const dbGames = new Datastore({ filename: './game.db', autoload: true })

fastify.get('/hello', (request, reply) => {
  reply.send({ hello: 'world' })
})

fastify.get('/problem', (request, reply) => {
  dbProblem.find({}, (err, docs) => {
    reply.send(docs)
  })
})

fastify.get('/answer/:id', (request, reply) => {
  dbAnswer.findOne({ _id: request.params.id }, (err, doc) => {
    reply.send(doc)
  })
})

fastify.get('/problem/:id', (request, reply) => {
  dbProblem.findOne({ _id: request.params.id }, (err, doc) => {
    reply.send(doc)
  })
})

fastify.post('/answer', (req, res) => {
  const { content, author, questionId } = req.body
  dbAnswer.insert({ content, author, creationDate: new Date(), points: 0 }, (err, newDoc) => {
    dbProblem.update({ _id: questionId }, { $push: { answers: newDoc._id } }, {}, () => {
      res.code(201)
      res.send(newDoc)
    })
  })
})

fastify.post('/problem', (req, res) => {
  const { title, content, author, gameId, tags } = req.body
  dbProblem.insert(
    { title, content, author, gameId, creationDate: new Date(), answers: [], stars: 0, tags },
    (err, newDoc) => {
      res.code(201)
      res.send(newDoc)
    }
  )
})

fastify.patch('/star/:id', (req, res) => {
  dbProblem.update({ _id: req.params.id }, { $inc: { stars: 1 } }, {}, () => {
    res.code(204)
    res.send()
  })
})

fastify.patch('/upvote/:id', (req, res) => {
  dbAnswer.update({ _id: req.params.id }, { $inc: { points: 1 } }, {}, () => {
    res.code(204)
    res.send()
  })
})

fastify.get('/game', (req, res) => {
  dbGames.find({}, (err, docs) => {
    res.send(docs)
  })
})

fastify.get('/game/:id', (req, res) => {
  dbGames.findOne({ id: Number(req.params.id) }, (err, doc) => {
    res.send(doc)
  })
})

fastify.listen(3000, err => {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${fastify.server.address().port}`)
})

fastify.use(cors())
