import React from 'react'
import { Media } from 'reactstrap';
import { Link } from 'react-router-dom';

const ProblemEntry = ({problem, game}) => {
    return (
      <Link to={`/community/problem/${problem._id}`} className="problem-entry" style={{ textDecoration: 'none' }}>
        <Media >
          <Media left href="#">
            {game &&
              <Media object src={game.image} alt="Generic placeholder image" />
            }
          </Media>
          <Media body>
            <Media heading>
              {problem.title}
              </Media>
              {game &&
                <h6 className="media-game">{game.title}</h6>
              }
              <div className="media-date">
                {Number((new Date() - new Date(problem.creationDate)) / 3600000).toFixed(0)}{' '}
                godziny temu
              </div>
            </Media>
            <div className="like">
              <i className="fa fa-heart" />
              <h5>{problem.stars}</h5>
            </div>
        </Media>
      </Link>
  )}

  export default ProblemEntry