import React, { Component } from 'react'
import { InputGroup } from 'reactstrap'
import { InstantSearch, Hits, SearchBox, Highlight } from 'react-instantsearch/dom'
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import './search.scss'

const GameCard = styled(Card)`
  border: none;
`

const Search = () => (
  <div className="container">
    <Hits hitComponent={Product} />
  </div>
)

const SearchInput = props => <div className="col-sm-12">{props.children}</div>

const Product = ({ hit }) => {
  return (
    <Link to={`/community/game/${hit.id}`}>
      <GameCard>
        <CardImg top width="100%" src={hit.image} alt={hit.title} />
        <CardBody>
          <CardTitle>{hit.title}</CardTitle>
        </CardBody>
      </GameCard>
    </Link>
  )
}
export default class SearchComponent extends Component {
  render() {
    return (
      <div className="search-view">
        <InstantSearch
          appId="YN0UGK76HW"
          apiKey="367574dcc0fe662127dbfbe8572cb1be"
          indexName="gog_games">
          <SearchInput>
            <SearchBox />
          </SearchInput>
          <Search hitComponent={Product} />
        </InstantSearch>
      </div>
    )
  }
}
