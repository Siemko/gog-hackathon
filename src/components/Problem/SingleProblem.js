import React, { Component } from 'react'
export default class SingleProblem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      starredByU: false,
    }
  }

  starProblem = () => {
    this.props.starProblem(this.props.problem._id)
    this.setState(prevState => {
      return { starredByU: !prevState.starredByU }
    })
  }
  render() {
    return (
      <div className="single-problem">
        <h3 className="game-title">
          Witcher
        </h3>
        <div className="author-info">
          <span className="avatar">
            <i className="fa fa-user-alt" />
          </span>
          <h4>{this.props.problem.author}</h4>
          <h5>
            {Number((new Date() - new Date(this.props.problem.creationDate)) / 3600000).toFixed(0)}{' '}
            godziny temu
          </h5>
        </div>
        <div className="problem-details">
          <div
            style={{ color: this.state.starredByU ? 'rgba(255, 75, 75, 0.8)' : '#ccc' }}
            className="rating"
            onClick={this.starProblem.bind(this)}>
            <h4>{this.props.problem.stars}</h4>
            <i className={this.state.starredByU ? 'fa fa-heart' : 'far fa-heart'} />
          </div>
          <h1>{this.props.problem.title}</h1>
          <div dangerouslySetInnerHTML={{ __html: this.props.problem.content }} />
        </div>
      </div>
    )
  }
}
