import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Row } from 'reactstrap'
import './problem.scss'
import SingleProblem from './SingleProblem'
import Answers from './Answers'
import AnswerInput from './AnswerInput'
class Problem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      singleProblem: null,
    }
  }

  componentDidMount() {
    this.props.getProblems()
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const singleProblemId = nextProps.match.params.id
    const singleProblem = nextProps.problemState.find(p => p._id == singleProblemId)
    if (singleProblem === prevState) {
      return null
    }
    return {
      singleProblem,
    }
  }

  render() {
    if (!this.state.singleProblem) return null
    return (
      <Row className="problem-page">
        <div className="col-md-8">
          <SingleProblem problem={this.state.singleProblem} starProblem={this.props.starProblem} />
          <Answers {...this.state.singleProblem} />
          <AnswerInput questionId={this.state.singleProblem._id}/>
        </div>
        <div className="col-md-4">
          <div className="tags">
            <h5>Tagi</h5>
            <span>Wiedzmin 3</span>
            <span>Wyspa mgieł</span>
            <span>Ciri</span>
            <span>Cyberpunk</span>
          </div>
          <div className="related">
            <h5>Podobne tematy</h5>
            <ul />
          </div>
        </div>
      </Row>
    )
  }
}

const mapState = state => ({
  problemState: state.problem,
  gameState: state.game
})

const mapDispatch = dispatch => ({
  getProblems: () => dispatch.problem.getProblems(),
  starProblem: problemId => dispatch.problem.starProblem(problemId),
})

export default withRouter(connect(mapState, mapDispatch)(Problem))
