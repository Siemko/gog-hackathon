import React, { Component } from 'react'
import './answers.scss'
import { connect } from 'react-redux'
export default class Answers extends Component {
  render() {
    const { answers } = this.props
    return (
      <div className="answer-list">
        {!!answers.length && <h2>Najlepsza odpowiedź</h2>}
        {answers.map((answer, index) => <Answer key={answer._id} {...answer} index={index} />)}
      </div>
    )
  }
}

class Answer extends Component {
  upvoteAnswer = () => {
    this.props.upvote(this.props._id)
    this.props.getProblems()
  }
  render() {
    return (
      <div
        className="single-answer"
        style={{ backgroundColor: this.props.index == 0 ? '#fdedd3' : 'transparent' }}>
        <div className="rating">
          <i className="fa fa-chevron-up" onClick={this.upvoteAnswer.bind(this)} />
          <h4>{this.props.points}</h4>
        </div>
        <div className="author-info">
          <span className="avatar">
            <i className="fa fa-user-alt" />
          </span>
          <h4>{this.props.author}</h4>
          <h5>
            {Number((new Date() - new Date(this.props.creationDate)) / 3600000).toFixed(0)} godzin
            temu
          </h5>
        </div>
        <div className="answer-details" dangerouslySetInnerHTML={{ __html: this.props.content }} />
      </div>
    )
  }
}

const mapDispatch = dispatch => ({
  upvote: answerId => dispatch.answer.upvoteAnswer(answerId),
  getProblems: () => dispatch.problem.getProblems(),
})

Answer = connect(null, mapDispatch)(Answer)
