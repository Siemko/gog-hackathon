import React, { PureComponent } from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import './answer-input.scss'
import { Button } from 'reactstrap'
import { connect } from 'react-redux'
import debounce from 'lodash/debounce'
class AnswerInput extends PureComponent {
  state = {
    content: '',
  }

  handleChange = value => {
    this.setState({ content: value })
  }

  sendAnswer = debounce(() => {
    this.props.addAnswer({
      content: this.state.content,
      questionId: this.props.questionId,
      author: 'Geralt z Rivii',
    })
    setTimeout(() => {
        this.props.getProblems()
        this.setState({ content: '' })
    }, 1000)
  })

  render() {
    const modules = {
      toolbar: [
        [{ header: [1, 2, false] }],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
        ['link', 'image'],
        ['clean'],
      ],
    }

    const formats = [
      'header',
      'bold',
      'italic',
      'underline',
      'strike',
      'blockquote',
      'list',
      'bullet',
      'indent',
      'link',
      'image',
    ]
    return (
      <div className="answer-input">
        <ReactQuill
          value={this.state.content}
          onChange={this.handleChange}
          modules={modules}
          formats={formats}
        />
        <Button className="mt-3 float-right" onClick={this.sendAnswer}>
          Odpowiedz
        </Button>
      </div>
    )
  }
}

const mapDispatch = dispatch => ({
  addAnswer: answer => dispatch.answer.addAnswer(answer),
  getProblems: () => dispatch.problem.getProblems(),
})

export default connect(null, mapDispatch)(AnswerInput)
