import React, { Component } from 'react'
import ProblemEntry from '../Common/ProblemEntry';

export class GameProblems extends Component {
  constructor(props) {
    super(props)
    this.state = {
      sortBy: 'hot',
    }
  }

  render() {
    return (
      <div className="top-problems">
        <ul className="sort-by">
          <li className={this.state.sortBy == 'hot' ? 'active' : ''}>Gorące</li>
          <li className={this.state.sortBy == 'new' ? 'active' : ''}>Najnowsze</li>
          <li className={this.state.sortBy == 'treding' ? 'active' : ''}>Wschodzące</li>
          <li className={this.state.sortBy == 'best' ? 'active' : ''}>Najlepsze</li>
        </ul>
        {this.props.problems.map((prob, index) => {
            return <ProblemEntry key={index} problem={prob} game={this.props.game}/>
          })}
      </div>
    )
  }
}
