import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { Row } from 'reactstrap'
import { GameProblems } from './GameProblems'
import isEqual from 'lodash/isEqual'
import differenceWith from 'lodash/differenceWith'
import styled from 'styled-components'

const SectionTitle = styled.h5`
  font-size: 12px;
  font-weight: bold;
  color: #2d1b8e;
`

const Title = styled.h3`
  font-size: 24px;
  font-weight: bold;
  color: #282121;
`

class Problem extends Component {
  constructor(props) {
    super(props)
    this.state = {
      game: null,
      problems: [],
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const singleGameId = nextProps.match.params.id
    const game = nextProps.gameState.find(g => g.id == singleGameId)
    const problems = nextProps.problemState.filter(p => p.gameId == singleGameId)
    if (game === prevState.game && !differenceWith(problems, prevState.problems, isEqual).length) {
      return null
    }
    return {
      game,
      problems,
    }
  }

  componentDidMount() {
    this.props.getProblems()
    this.props.getGames()
  }

  render() {
    if (!this.state.game) return null
    return (
      <Row className="problem-page" style={{marginTop: '106px'}}>
        <div className="game-bg" style={{background: `url("${this.state.game.background}")`}}>></div>
        <div className="col-md-8" style={{position: 'relative', zIndex: '10'}}>
          {!!this.state.problems.length && <GameProblems game={this.state.game} problems={this.state.problems} />}
          {!this.state.problems.length && <h2>Dla tej gry nie zadano jeszcze żadnego pytania</h2>}
        </div>
        <div className="col-md-4" style={{position: 'relative', zIndex: '10'}}>
          <SectionTitle>
            Opis gry
          </SectionTitle>
          <Title>{this.state.game.title}</Title>
          <img src={this.state.game.image} className="img-fluid" style={{borderRadius: '5px'}}/>
          <div className="tags">
            <h5>Atrybuty gry</h5>
            {this.state.game.features.map(f => <span key={f}>{f}</span>)}
          </div>
          <div className="related">
            <h5>Podobne tematy</h5>
            <ul />
          </div>
        </div>
      </Row>
    )
  }
}

const mapState = state => ({
  problemState: state.problem,
  gameState: state.game,
})

const mapDispatch = dispatch => ({
  getProblems: () => dispatch.problem.getProblems(),
  getGames: () => dispatch.game.getGames(),
})

export default withRouter(connect(mapState, mapDispatch)(Problem))
