import React, { Component } from 'react'
import { connect } from 'react-redux'

import './topProblems.scss';
import ProblemEntry from '../Common/ProblemEntry'

class TopProblems extends Component {
  constructor(props) {
    super(props)
    this.state = {
      sortBy: 'hot'
    }
  }

  componentDidMount() {
    this.props.getProblems()
    this.props.getGames()
  }
  render() {
    const { problemState, gameState } = this.props
    if (!problemState || !gameState) return null

    return (
      <div className="top-problems">
        <ul className="sort-by">
          <li className={this.state.sortBy == 'hot' ? 'active' : ''}>Gorące</li>
          <li className={this.state.sortBy == 'new' ? 'active' : ''}>Najnowsze</li>
          <li className={this.state.sortBy == 'treding' ? 'active' : ''}>Wschodzące</li>
          <li className={this.state.sortBy == 'best' ? 'active' : ''}>Najlepsze</li>
        </ul>
        {problemState.map((prob, index) => {
          const game = gameState.find(g => prob.gameId == g.id)
          return <ProblemEntry key={index} problem={prob} game={game}/>
        })}
      </div>
    )
  }
}

const mapState = state => ({
  problemState: state.problem,
  gameState: state.game
})

const mapDispatch = dispatch => ({
  getProblems: () => dispatch.problem.getProblems(),
  getGames: () => dispatch.game.getGames()
})

export default (TopProblems = connect(mapState, mapDispatch)(TopProblems))
