import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import './top-games.scss'

class TopGames extends Component {
    render() {
        const { gameState } = this.props
        if (!gameState) return null
        const topGames = gameState.slice(0,9)
        return (
            <Fragment>
                <h5 className="top-games-title">Najpopularniejsze gry</h5>
                <ul className="top-games-list">
                    {topGames && topGames.map(g => (
                        <TopGame key={g.id} {...g} />
                    ))}
                </ul>
            </Fragment>
        )
    }
}

const TopGame = (game) => (
    <Link to={`community/game/${game.id}`} style={{ textDecoration: 'none' }} className="top-game">
        <img src={game.image} />
        <span>{game.title}</span>
    </Link>
)

const mapState = state => ({
    gameState: state.game
})

export default (TopGames = connect(mapState)(TopGames))
