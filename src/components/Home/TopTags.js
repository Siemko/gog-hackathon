import React from 'react'
import styled from 'styled-components'

const Title = styled.h5`
    font-size: 12px!important;
    font-weight: bold;
    color: #2d1b8e!important;
    text-transform: uppercase;
`

const Tags = styled.div`
    margin-top: 0;
    margin-bottom: 40px;
`

const TopTags = () => (
    <Tags className="tags">
        <Title>Popularne tagi</Title>
        <span>Wiedzmin 3</span>
        <span>Wyspa mgieł</span>
        <span>Ciri</span>
        <span>Cyberpunk</span>
    </Tags>
)

export default TopTags