import { init } from '@rematch/core'
import { count } from './modules/counter/counter'
import { message } from './modules/message/message'
import { post } from './modules/post/post'
import { problem } from './modules/problem/problem'
import { answer } from './modules/answer/answer'
import { game } from './modules/game/game'
const store = init({
  models: {
    count,
    message,
    post,
    problem,
    answer,
    game,
  },
})

export default store
