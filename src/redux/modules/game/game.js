export const game = {
    state: [], // initial state
    reducers: {
      setGames(state, payload) {
        return [...state, ...payload]
      },
    },
    effects: {
      // handle state changes with impure functions.
      // use async/await for async actions
      async getGames() {
        this.setGames(await (await fetch(`http://localhost:3000/game`)).json())
      },
    },
  }
  