export const problem = {
  state: [], // initial state
  reducers: {
    setProblems(state, payload) {
      return payload
    },
    addProblem(state, payload) {
      return { ...state, payload }
    },
  },
  effects: {
    // handle state changes with impure functions.
    // use async/await for async actions
    async getProblems() {
      const problems = await (await fetch('http://localhost:3000/problem')).json()
      let res = []
      for (let problem of problems) {
        let answers = []
        for (let answer of problem.answers) {
          answers.push(await (await fetch(`http://localhost:3000/answer/${answer}`)).json())
        }
        res.push(Object.assign({}, problem, { answers: answers }))
      }
      this.setProblems(res)
    },
    async addProblemAsync(problem) {
      this.addProblem(
        await (await fetch('http://localhost:3000/problem', {
          method: 'POST',
          body: JSON.stringify(problem),
          headers: {
            'content-type': 'application/json',
          },
        })).json()
      )
    },
    async starProblem(problemId) {
      await fetch(`http://localhost:3000/star/${problemId}`, {
        method: 'PATCH',
      })
      await this.getProblems()
    },
  },
}
