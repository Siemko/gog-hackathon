export const post = {
  state: [], // initial state
  reducers: {
    setPosts(state, payload) {
      return payload
    },
    addPost(state, payload) {
      return { ...state, payload }
    },
  },
  effects: {
    // handle state changes with impure functions.
    // use async/await for async actions
    async getPosts() {
      this.setPosts(await (await fetch('http://localhost:3000/post')).json())
    },
    async addPostAsync(post) {
      this.addPost(
        await (await fetch('http://localhost:3000/post', {
          method: 'POST',
          body: JSON.stringify(post),
          headers: {
            'content-type': 'application/json',
          },
        })).json()
      )
    },
  },
}
