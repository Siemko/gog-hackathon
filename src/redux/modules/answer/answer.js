export const answer = {
  state: {}, // initial state
  reducers: {},
  effects: {
    async addAnswer(answer) {
      await (await fetch('http://localhost:3000/answer', {
        method: 'POST',
        body: JSON.stringify(answer),
        headers: {
          'content-type': 'application/json',
        },
      })).json()
    },
    async upvoteAnswer(answerId) {
      await fetch(`http://localhost:3000/upvote/${answerId}`, {
        method: 'PATCH',
      })
    },
  },
}
