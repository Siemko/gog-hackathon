import React, { PureComponent, Fragment } from 'react'
import { connect } from 'react-redux'
import { Button } from 'reactstrap'
import { Formik } from 'formik'

class Example extends PureComponent {
  componentDidMount() {
    this.props.getPosts()
  }
  render() {
    return (
      <Fragment>
        <div>
          <h1>{this.props.message}</h1>
          <Button color="primary" onClick={() => this.props.setMessage('ELO!')}>
            Change message!
          </Button>
        </div>
        <Formik
          initialValues={{
            title: '',
            content: '',
          }}
          onSubmit={(values, { setSubmitting, setErrors }) => {
            this.props.addPost(values).then(
              () => {
                setSubmitting(false)
                this.props.getPosts()
              },
              errors => {
                setSubmitting(false)
                setErrors(errors)
              }
            )
          }}
          render={({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <input
                type="text"
                name="title"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.title}
              />
              {touched.title && errors.title && <div>{errors.title}</div>}
              <input
                type="text"
                name="content"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              {touched.content && errors.content && <div>{errors.content}</div>}
              <button type="submit" disabled={isSubmitting}>
                Submit
              </button>
            </form>
          )}
        />
        <ul>
          {this.props.post.length && this.props.post.map(p => {
            return (
              <li key={p._id}>
                <h2>{p.title}</h2>
                <p>{p.content}</p>
              </li>
            )
          })}
        </ul>
      </Fragment>
    )
  }
}

const mapState = state => ({
  message: state.message,
  post: state.post,
})

const mapDispatch = dispatch => ({
  setMessage: msg => dispatch.message.setMessage(msg),
  getPosts: () => dispatch.post.getPosts(),
  addPost: post => dispatch.post.addPostAsync(post),
})

export const Other = connect(mapState, mapDispatch)(Example)
