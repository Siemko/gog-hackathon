import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { Button } from 'reactstrap'
class Count extends PureComponent {
  state = {
    msg: 'Hello!',
  }

  render() {
    return (
      <div>
        <h1>{this.state.msg}</h1>
        <p>The count is {this.props.count}</p>
        <Button color="primary" onClick={this.props.increment}>
          increment
        </Button>
        <Button color="primary" onClick={this.props.incrementAsync}>
          incrementAsync
        </Button>
      </div>
    )
  }
}

const mapState = state => ({
  count: state.count,
})

const mapDispatch = ({ count }) => ({
  increment: () => count.increment(1),
  incrementAsync: () => count.incrementAsync(1),
})

export const Counter = connect(mapState, mapDispatch)(Count)
