import React, { PureComponent } from "react";
import TopProblems from "../components/Home/TopProblems";
import TopTags from './../components/Home/TopTags';
import TopGames from './../components/Home/TopGames';
export default class CommunityView extends PureComponent {
    render () {
        return (
            <div className="container">
                <div className="row" style={{marginTop: '100px'}}>
                    <div className="col-md-8">
                        <TopProblems />
                    </div>
                    <div className="col-md-4">
                        <TopTags />
                        <TopGames />
                    </div>
                </div>
            </div>
        )
    }
}