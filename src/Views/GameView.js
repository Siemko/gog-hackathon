import React, { Component } from 'react'
import Game from '../components/Game/Game';

export default class GameView extends Component {
  render() {
    return (
      <div>
        <Game />
      </div>
    )
  }
}
