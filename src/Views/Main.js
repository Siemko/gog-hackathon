import React from 'react'
import { Header } from '../components/Header/Header'
import { Route, Switch, Redirect } from 'react-router-dom'
import { Container } from 'reactstrap'

import ProblemView from './ProblemView';
import GameView from './GameView';
import CommunityView from './CommunityView';
import SearchView from './SearchView';

export const Main = () => (
  <div className="app">
    <Header />
    <Container>
      <Switch>
        <Route exact path="/community" name="Community" component={CommunityView} />
        <Route path="/community/search" name="Search" component={SearchView} />
        <Route path="/community/game/:id" name="Game" component={GameView} />
        <Route path="/community/problem/:id" name="Problem" component={ProblemView} />
        <Redirect from="/" exact to="/community" />
      </Switch>
    </Container>
  </div>
)
