import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import store from './redux'
import { Router, Route, Switch } from 'react-router-dom'
import customHistory from './redux/history'
import 'bootstrap/scss/bootstrap.scss'
import { Main } from './Views/Main';

import './styles/global.scss';

ReactDOM.render(
  <Provider store={store}>
    <Router history={customHistory}>
      <Switch>
        <Route path="/" component={Main} />
      </Switch>
    </Router>
  </Provider>,

  document.getElementById('app')
)

module.hot.accept()
